// Zad 8.1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
volatile DWORD dwStart, result_s, result_w, suma_s, suma_w, min_s, min_w, max_s, max_w;
double pi_s(int num_steps) 
{
	int i;
	double x, pi, sum = 0.0, step;
	step = 1.0 / (double)num_steps;
	for (i = 1; i <= num_steps; i++) 
	{
		x = (i - 0.5) * step;
		sum = sum + 4.0 / (1.0 + x*x);
	}
	pi = step * sum;
	return pi;
}
double pi_w(int num_steps) {
	int i;
	double x, pi, sum = 0.0, step;
	step = 1.0 / (double)num_steps;
	#pragma omp parallel for reduction(+:sum) private(x)
	for (i = 1; i <= num_steps; i++) 
	{
		x = (i - 0.5) * step;
		sum = sum + 4.0 / (1.0 + x*x);
	}
	pi = step * sum;
	return pi;
}
int main(int argc, char* argv[]) 
{
	double d, min_ratio, max_ratio, suma_ratio, ratio;
	ratio = 0;
	int i, n = 10000000;
	
	for (i = 0; i < 10; i++)
	{
		dwStart = GetTickCount();
		d = pi_s(n);
		result_s = GetTickCount() - dwStart;
		printf_s("S: pi = %.15f, czas: %d ms\n", d, result_s);
		if (i == 0)
		{
			suma_s=min_s = max_s = result_s;
			
		}
		else
		{
			if (result_s < min_s) min_s = result_s;
			if (result_s > max_s) max_s = result_s;
			suma_s += result_s;
		}
		
		dwStart = GetTickCount();
		d = pi_w(n); 
		result_w = GetTickCount() - dwStart;
		printf_s("W: pi = %.15f, czas: %d ms\n", d, result_w);
		if (i == 0)
		{
			suma_w = min_w = max_w = result_w;

		}
		else
		{
			if (result_w < min_w) min_w = result_w;
			if (result_w > max_w) max_w = result_w;
			suma_w += result_w;
		}
		
		ratio = ((double)result_s / (double)result_w);
		printf_s("Przyspieszenie: %.2f\n\n",ratio);
		if (i == 0)
		{
			min_ratio=max_ratio=suma_ratio = ratio;
		}
		else
		{
			if (ratio < min_ratio) min_ratio = ratio;
			if (ratio > max_ratio) max_ratio = ratio;
			suma_ratio += ((double)result_s / (double)result_w);
		}
	}
	printf_s("Ilosc krokow: %d\n", n);
	printf_s("Ilosc pomiarow: %d\n\n", i);
	printf_s("Min czas sekwencyjny: %d\n", min_s);
	printf_s("Max czas sekwencyjny: %d\n", max_s);
	printf_s("Sredni czas sekwencyjny: %.2f\n\n", ((double)suma_s / (double)i));
	printf_s("Min czas wspolbiezny: %d\n", min_w);
	printf_s("Max czas wspolbiezny: %d\n", max_w);
	printf_s("Sredni czas wspolbiezny: %.2f\n\n", ((double)suma_w / (double)i));
	printf_s("Min przyspieszenie: %.2f\n", min_ratio);
	printf_s("Max przyspieszenie: %.2f\n", max_ratio);
	printf_s("Srednie przyspieszenie: %.2f\n\n", (suma_ratio / (double)i));

	system("pause");
	return 0;
}